from math import *

class Vector:
    
    def __init__(self,x,y,z):
        if type(x)==int:
            x=float(x)
        if type(y)==int:
            y=float(y)
        if type(z)==int:
            z=float(z) 
        if type(x)==float and type(y)==float and type(z)==float:
            self.__x = x
            self.__y = y
            self.__z = z
        else:
            return "Error"
        
    def get_x(self):
        return self.__x
    
    def set_x(self,x):
        self.__x=x

    def get_y(self):
        return self.__y

    def set_y(self,y):
        self.__y=y

    def get_z(self):
        return self.__z

    def set_z(self,z):
        self.__z=z

    def __str__(self):
        return "("+str(self.__x)+","+str(self.__y)+","+str(self.__z)+")"
    
    def length(self):
        self.__length=sqrt((self.__x**2)+(self.__y**2)+(self.__z**2))
        return self.__length

    def __add__(self, other):
        if isinstance(other,Vector):
            return(self.__x+other.get_x(), self.__y+other.get_y(),self.__z+other.get_z())
        else:
            return "Error"

    def __sub__(self, other):
        if isinstance(other,Vector):
            return(self.__x-other.get_x(), self.__y-other.get_y(),self.__z-other.get_z())
        else:
            return "Error"

    def __mul__(self, other):
        if type(other)==Vector:
            return (self.__x*other.get_x()+self.__y*other.get_y()+self.__z*other.get_z())
        if type(other)==int:
            return (self.__x*other,self.__y*other,self.__z*other)
        else:
            return "Error"
        
    def __rmul__(self, other):
        if type(other)==Vector:
            return (self.__x*other.get_x()+self.__y*other.get_y()+self.__z*other.get_z())
        if type(other)==int:
            return (self.__x*other,self.__y*other,self.__z*other)
        else:
            return "Error"
        
    def __xor__(self, other):
        if isinstance(other, Vector):
            return Vector(self.__y*other.get_z()-self.__z*other.get_y(),-(self.__x*other.get_z()-self.__z*other.get_x()),self.__x*other.get_y()-self.__y*other.get_x())
        else:
            return "Error"
        
    def angle (self, other):
        if isinstance(other, Vector):
            alpha=acos(self.__mul__(other)/(self.length()*other.length()))
        return degrees(alpha)


v1=Vector(1,2,3)
v2=Vector(3,2,1)
#v3=Vector("a","b","c")
print("v1="+str(v1))
print("v2="+str(v2))
print("v1+v2="+str(v1+v2))
print("v1-v2="+str(v2-v1))
print("v1*v2="+str(v1*v2))
print("v1*5="+str(v1*5))
print("5*v1="+str(5*v1))
print("v1 and v2 vector product="+str(v1^v2))
#print("v1+v3="+str(v1+v3))
print("v1 and v2 constituted angle="+str(v1.angle(v2)))